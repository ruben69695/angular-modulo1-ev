# Especialización en desarrollo FullStack
Desarrollo FullStack por la universidad Austral, cursado en Coursera.

## Desarrollo de páginas con Angular - Evaluación del proyecto - Módulo 1

### Ejercicios
1. Utilizar el ecosistema básico de Angular y el concepto de componentes
2. Desarrollar una primera SPA básica con estilos de Bootstrap
3. Utilizar el lenguaje Typescript

---

### Todo web application
Una aplicación web responsive desarrollada principalmente con el framework de Angular con una lista de tareas por hacer

### Para empezar

Para satisfacer las dependencias del proyecto, ejecute:
```bash
npm install
```

Para arrancar la aplicación
```bash
ng serve
```

### Tecnologías
- HTML / CSS / JS
- Angular
- TypeScript
- Bootstrap 4
- NodeJS
- Visual Studio Code
- Git
- Bitbucket