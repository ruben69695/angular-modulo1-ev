export class TodoItem {
    name: string;
    creationDate: Date;

    constructor(name: string, creationDate: Date) {
        this.name = name;
        this.creationDate = creationDate;
    }

}